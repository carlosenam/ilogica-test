<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ilogica
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="widget-area">
			<div class="col">
				<?php the_custom_logo(); ?>
			</div>
			<div class="col">
				<?php dynamic_sidebar( 'footer1' ); ?>
			</div>
			<div class="col">
				<?php dynamic_sidebar( 'footer2' ); ?>
			</div>
		</div>
		<div class="wrapper">
			<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-footer',
						'menu_id'        => 'footer-menu',
					)
				);
			?>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
