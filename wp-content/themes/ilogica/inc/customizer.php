<?php
/**
 * Ilogica Theme Customizer
 *
 * @package Ilogica
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function ilogica_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'ilogica_customize_partial_blogname',
		) );
	}
}
add_action( 'customize_register', 'ilogica_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function ilogica_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function ilogica_customize_preview_js() {
	wp_enqueue_script( 'ilogica-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'ilogica_customize_preview_js' );
