/* jshint node:true */
module.exports = function( grunt ) {
	'use strict';

	var sass = require( 'node-sass' );

	grunt.initConfig({

		// Autoprefixer.
		postcss: {
			options: {
				processors: [
					require( 'autoprefixer' )({
						browsers: [
							'> 0.1%',
							'ie 8',
							'ie 9'
						]
					})
				]
			},
			dist: {
				src: [
					'style.css'
				]
			}
		},

		// Compile all .scss files.
		sass: {
			dist: {
				options: {
					implementation: sass,
					sourceMap: false
				},
				files: [{
					'style.css': 'sass/style.scss',
					'css/slider.css': 'sass/slider.scss'
				}]
			}
		},

		// Minify all .css files.
		cssmin: {
			main: {
				files: {
					'style.css': ['style.css'],
					'css/slider.css': ['css/slider.css']
				}
			},
		},

		// Minify .js files.
		uglify: {
			options: {
				output: {
					comments: 'some'
				}
			},
			main: {
				files: [{
					expand: true,
					cwd: 'js/',
					src: [
						'*.js',
						'!*.min.js'
					],
					dest: 'js/',
					ext: '.min.js'
				}]
			},
		},

		// Watch changes for assets.
		watch: {
			css: {
				files: [
					'sass/**/*.scss'
				],
				tasks: [
					'sass',
					'css'
				]
			},
			js: {
				files: [
					// js
					'js/*.js',
					'!js/*.min.js'
				],
				tasks: [
					'uglify'
				]
			}
		},
		babel: {
			options: {
				presets: ['@wordpress/babel-preset-default']
			},
			dist: {
				files: {
					'./assets/js/editor.js': './assets/js/src/editor.js'
				}
			}
		}
	});

	// Load NPM tasks to be used here
	grunt.loadNpmTasks( 'grunt-contrib-uglify' );
	grunt.loadNpmTasks( 'grunt-sass' );
	grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	grunt.loadNpmTasks( 'grunt-postcss' );
	grunt.loadNpmTasks( 'grunt-babel' );


	// Register tasks
	grunt.registerTask( 'default', [
		'css',
		'uglify'
	]);

	grunt.registerTask( 'css', [
		'sass',
		'postcss',
		'cssmin'
	]);

	grunt.registerTask( 'dev', [
		'default'
	]);
};
