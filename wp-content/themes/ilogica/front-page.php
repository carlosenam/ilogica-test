<?php
/**
 * The template for displaying the front page of the site
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Ilogica
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section id="slider">
				<div class="owl-carousel owl-theme">
					<?php

					for ( $i = 1; $i <= 3; $i ++ ) {
						?>
							<div class="item">
								<div class="slide-wrapper slide<?php echo esc_html( $i ); ?>">
									<div class="slide-text">
										<?php the_field( 'texto_slide_' . $i ); ?>
									</div>
									<?php
									$slide_link = get_field( 'link_' . $i );

									if ( $slide_link ) {
										?>
											<div class="slide-cta">
												<a href="<?php the_field( 'link_' . $i ); ?>" target="_blank">
													<?php the_field( 'texto_link_' . $i ); ?>
												</a>
											</div>
										<?php
									}
									?>
								</div>
							</div>
						<?php
					}
					?>
				</div>
				<style>
					#slider .owl-carousel .slide1 {
						background-image: url( <?php the_field( 'imagen_slider_1' ); ?> )
					}

					#slider .owl-carousel .slide2 {
						background-image: url( <?php the_field( 'imagen_slider_2' ); ?> )
					}

					#slider .owl-carousel .slide3 {
						background-image: url( <?php the_field( 'imagen_slider_3' ); ?> )
					}

				</style>
			</section>
			<section id="last-posts">
				<div class="last-posts-title">
					<?php echo esc_html__( 'Últimas Entradas', 'ilogica' ) ?>
				</div>
				<div class="posts-wrapper">
					<?php
					$args = array(
						'posts_per_page' => get_field( 'numero_de_posts' ),
					);

					$query = new WP_Query( $args );
					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();
							?>
							<div class="ilogica-post">
								<div class="ilogica-post-title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</div>
								<div class="ilogica-post-exceprt">
									<?php echo esc_html( wp_trim_words( get_the_content(), 20, '...' ) ); ?>
								</div>
							</div>
							<?php
						}
						wp_reset_postdata();
					}
					?>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
