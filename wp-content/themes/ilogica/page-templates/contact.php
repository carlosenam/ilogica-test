<?php
/**
 * Template Name: Contacto
 *
 * @package ilogica
 */

get_header();
if ( have_posts() ) {
	the_post();
	?>
	<div class="contact-content">
		<?php the_content(); ?>
	</div>
	<div class="contact-form">
		<?php echo do_shortcode( '[cf_form]' ); ?>
	</div>
	<?php
}
get_footer();
