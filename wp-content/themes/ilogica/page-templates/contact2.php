<?php
/**
 * Template Name: Contacto (Lado a Lado)
 *
 * @package ilogica
 */

get_header();
if ( have_posts() ) {
	the_post();
	?>
	<div class="contact-wrapper">
		<div class="contact-form">
			<?php echo do_shortcode( '[cf_form]' ); ?>
		</div>
		<div class="contact-content">
			<?php the_content(); ?>
		</div>
	</div>
	<?php
}
get_footer();