<?php
/**
 * Class CF_Form_Shortcode
 *
 * @package CF
 */

// Prevents direct access to this file.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * Handles the [CF_form] shortcode
 */
class CF_Form_Shortcode {

	/**
	 * Add the shortcode and register the ajax callback
	 *
	 * @return void
	 */
	public function __construct() {
		add_shortcode( 'cf_form', array( $this, 'html_output' ) );
		add_action( 'wp_ajax_cf_process_form_data', array( $this, 'process_form_data' ) );
		add_action( 'wp_ajax_nopriv_cf_process_form_data', array( $this, 'process_form_data' ) );
	}

	/**
	 * Shortcode HTML output
	 *
	 * @return string HTML output
	 */
	public function html_output() {
		// JS and CSS files are enqueued only when the shortcode is being used.
		$this->enqueue_scripts_and_styles();
		$form_data_id = CF()->generate_unique_id();

		ob_start();
		?>
		<div class="cf_form_parent" id="<?php echo esc_attr( $form_data_id ); ?>">
			<div id="qunit"></div>
			<div class="cf_loader hidden">
				<div class="cf_spinner"></div>
			</div>
			<div class="cf_result hidden">
				<div class="cf_result_msg">
					<p></p>
					<span class="cf_back">
						<?php echo esc_html__( 'Back', 'cf' ); ?>
					</span>
				</div>
			</div>
			<div class="cf_form_wrapper">
				<form class="cf_form">
					<div class="cf_form_title">
						<?php echo esc_html__( 'Formulario de Contacto', 'cf' ); ?>
					</div>
					<div class="cf_fields">
						<div class="cf_field">
							<label for="cf_first_name"><?php echo esc_html__( 'Nombre', 'cf' ); ?></label>
							<input type="text" id="cf_first_name" value="" />
						</div>
						<div class="cf_field">
							<label for="cf_last_name"><?php echo esc_html__( 'Apellido', 'cf' ); ?></label>
							<input type="text" id="cf_last_name" value="" />
						</div>
						<div class="cf_field">
							<label for="cf_email"><?php echo esc_html__( 'Correo', 'cf' ); ?></label>
							<input type="email" id="cf_email" value="" />
						</div>
						<div class="cf_field">
							<label for="cf_phone"><?php echo esc_html__( 'Teléfono', 'cf' ); ?></label>
							<input type="tel" id="cf_phone" value="" />
						</div>
						<div class="cf_field">
							<label for="cf_subject"><?php echo esc_html__( 'Asunto', 'cf' ); ?></label>
							<input type="text" id="cf_subject" />
						</div>
						<div class="cf_field">
							<label for="cf_message"><?php echo esc_html__( 'Mensaje', 'cf' ); ?></label>
							<textarea id="cf_message"></textarea>
						</div>
						<div class="cf_submit_wrapper">
							<button class="cf_submit_button"><?php echo esc_html__( 'Enviar', 'cf' ); ?></button>
						</div>
				</form>
				</div>
			</div>
		</div>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

	/**
	 * Enqueues the scripts and styles.
	 *
	 * @return void
	 */
	public function enqueue_scripts_and_styles() {
		wp_enqueue_script( 'cf-form', CF_ASSETS_URL . 'js/cf-form.js', array( 'jquery' ), CF_VERSION, true );
		wp_enqueue_style( 'cf-form-styles', CF_ASSETS_URL . 'css/cf-form.css', null, CF_VERSION );

		wp_localize_script(
			'cf-form',
			'CFFormVars',
			array(
				'ajaxUrl'           => admin_url( 'admin-ajax.php' ),
				'nonce'             => wp_create_nonce( 'cf-process-form-data' ),
				'successText'       => esc_html__( 'Gracias por enviarnos su mensaje', 'cf' ),
				'systemErrorText'   => esc_html__( 'OCurrió un error en el sistema', 'cf' ),
				'requiredFieldText' => esc_html__( 'Este campo es obligatorio', 'cf' ),
				'tooLong500Text'    => esc_html__( 'Muy Largo (max 500 caracteres)', 'cf' ),
				'tooLong100Text'    => esc_html__( 'Muy Largo (max 100 caracteres)', 'cf' ),
				'invalidEmailText'  => esc_html__( 'Correo Inválido', 'cf' ),
				'invalidPhone'      => esc_html__( 'Teléfono Inválido', 'cf' ),
			)
		);

		// ONLY FOR TESTING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!.
		//wp_enqueue_script( 'qunit', CF_URL . 'node_modules/qunit/qunit/qunit.js', array( 'jquery' ), CF_VERSION, true );
		//wp_enqueue_script( 'cf_form_tests', CF_URL . 'tests/js/test-cf-form.js', array( 'qunit', 'cf-form' ), CF_VERSION, true );
		//wp_enqueue_style( 'qunit-styles', CF_URL . 'node_modules/qunit/qunit/qunit.css', null, CF_VERSION );

	}

	/**
	 * Validates a single form field
	 *
	 * @param string $name Id of the form field.
	 * @param string $value Value of the field.
	 * @return array If the value of the field is valid then it returns an array
	 * with the validation result and the value itself, otherwise it return
	 * the validation message.
	 *
	 * Example:
	 * Invalid field => array( 'result' => 'error', 'message' => 'Este campo es obligatorio' )
	 * Valid field => array( 'result' => 'ok', 'message' => 'Alvarez' )
	 */
	public function validate_single_form_field( $name, $value ) {

		$result = array(
			'result'  => 'ok',
			'message' => $value,
		);

		switch ( $name ) {
			case 'first_name':
			case 'last_name':
			case 'subject':
			case 'message':
				if ( empty( $value ) ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'Este campo es obligatorio', 'cf' );
				} elseif ( 'message' === $name ) {
					if ( strlen( $value ) > 500 ) {
						$result['result']  = 'error';
						$result['message'] = esc_html__( 'Muy Largo (max 500 caracteres)', 'cf' );
					}
				} elseif ( strlen( $value ) > 100 ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'Muy Largo (max 100 caracteres)', 'cf' );
				}
				break;

			case 'phone':
				if ( empty( $value ) ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'Este campo es obligatorio', 'cf' );
				} elseif ( ! is_numeric( $value ) ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'Teléfono Inválido', 'cf' );
				}
				break;

			case 'email':
				if ( empty( $value ) ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'Este campo es obligatorio', 'cf' );
				} elseif ( ! is_email( $value ) ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'Correo Inválido', 'cf' );
				} elseif ( strlen( $value ) > 100 ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'Muy Largo (max 100 caracteres)', 'cf' );
				}
				break;
		}

		return $result;
	}

	/**
	 * Validates and sanitizes the data from the [cf_form] form.
	 *
	 * @param array $post_data This array corresponds to $_POST.
	 * @return array If there are invalid field values then it returns
	 * an array with validation result and the validation
	 * messages, otherwise it returns the field values (with no 'cf_' sufix)
	 *
	 * Example:
	 * Invalid Fields => array( 'result' => 'error', 'fields' => array( 'cf__first_name' => 'Este campo es obligatorio', ... ) )
	 * Valid field => array( 'result' => 'ok', 'fields' => array( 'first_name' => 'Carlos', ... ) )
	 */
	public function validate_form_data( $post_data ) {
		$form_fields = array(
			'result' => 'ok',
			'fields' => array(
				'first_name' => isset( $post_data['cf_first_name'] ) ? sanitize_text_field( $post_data['cf_first_name'] ) : '',
				'last_name'  => isset( $post_data['cf_last_name'] ) ? sanitize_text_field( $post_data['cf_last_name'] ) : '',
				'email'      => isset( $post_data['cf_email'] ) ? $post_data['cf_email'] : '',
				'phone'      => isset( $post_data['cf_phone'] ) ? sanitize_text_field( $post_data['cf_phone'] ) : '',
				'subject'    => isset( $post_data['cf_subject'] ) ? sanitize_text_field( $post_data['cf_subject'] ) : '',
				'message'    => isset( $post_data['cf_message'] ) ? sanitize_textarea_field( $post_data['cf_message'] ) : '',
			),
		);

		$validation_messages = array(
			'result' => 'error',
			'fields' => array(),
		);

		$fields = $form_fields['fields'];

		foreach ( $fields as $field_key => $field_value ) {
			$field_validation_result = $this->validate_single_form_field( $field_key, $field_value );

			if ( 'error' === $field_validation_result['result'] ) {
				$validation_messages['fields'][ 'cf_' . $field_key ] = $field_validation_result['message'];
			}
		}

		if ( ! empty( $validation_messages['fields'] ) ) {
			return $validation_messages;
		}

		return $form_fields;
	}

	/**
	 * Proccesses the data sent from the form
	 *
	 * @return void
	 */
	public function process_form_data() {
		check_ajax_referer( 'cf-process-form-data', 'nonce', false );

		$validation_result = $this->validate_form_data( $_POST );

		if ( 'error' === $validation_result['result'] ) {
			wp_send_json( $validation_result );
		}

		$cf_data = new CF_Data( $validation_result['fields'] );

		$cf_data->save();

		wp_send_json(
			array(
				'result' => 'ok',
			)
		);
	}

}
