<?php
/**
 * Class CF_Data
 *
 * @package CF
 */

// Prevents direct access to this file.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * Represents a row in the 'CF_form_data' database table
 */
class CF_Data {

	/**
	 * Form data
	 *
	 * @var array
	 */
	private $data = array(
		'first_name' => '',
		'last_name'  => '',
		'phone'      => '',
		'email'      => '',
		'subject'    => '',
		'message'    => '',
	);

	/**
	 * Set the initial form data
	 *
	 * @param array $data Array containing the form data.
	 * @return void
	 */
	public function __construct( $data = array() ) {
		if ( ! empty( $data ) ) {
			$this->set_data( $data );
		}
	}

	/**
	 * Set the form data
	 *
	 * @param array $data Array containing the form data.
	 * @return void
	 * @throws Exception When $data is not an array.
	 */
	public function set_data( $data ) {
		if ( gettype( $data ) !== 'array' ) {
			throw new Exception( '$data is not an array' );
		}

		foreach ( $this->data as $key => $value ) {
			if ( isset( $data[ $key ] ) ) {
				$this->data[ $key ] = $data[ $key ];
			}
		}
	}

	/**
	 * Saves a form data entry in the database
	 *
	 * @return int ID of the inserted row
	 * @throws Exception When the form data can be saved in the database.
	 */
	public function save() {
		global $wpdb;

		$data_to_save         = $this->data;
		$data_to_save['date'] = current_time( 'Y-m-d H:i:s' );

		$form_data_id = $wpdb->insert( CF()->get_table_name(), $data_to_save );
		if ( ! $form_data_id ) {
			throw new Exception( 'Database error: Form Data could not be saved' );
		}

		return $wpdb->insert_id;
	}

	/**
	 * Get the enquiry data (ONLY FOR TESTING!)
	 *
	 * @return array
	 */
	public function get_data() {
		return $this->data;
	}

}
