var $ = jQuery;
var CFActiveFormId = ''; // Id of the currently active enquiry form.
var CFActiveFormSelector = ''; // Selector of the currently active form.

/**
 * Set the Id of the active form
 *
 * @param {string} cfFormId ID of the active form
 */
function setActiveFormId(cfFormId) {
  CFActiveFormId = cfFormId;
  CFActiveFormSelector = '.cf_form_parent#' + cfFormId + ' ';
}

/**
 * Returns true if email is a valid email string, false otherwise.
 *
 * @param {string} email email to be evaluated.
 * @returns boolean
 */
function CFIsEmail(email) {
  var trimmedEmail = email.trim();
  if (trimmedEmail.match(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}/)) {
    return true;
  }
  return false;
}

/**
 * Checks if a string only have numbers
 * 
 * @param {string} phone
 * @returns boolean
 */
function CFIsNumeric(phone) {
  return /^\d+$/.test(phone);
}

/**
 * Validates a single form field
 *
 * @param {string} id Id of the field element
 * @param {string} value Value of the field element
 *
 * @return {object} if the field value is valid then it returns an object
 * with the field valud, otherwise it returns an object with the
 * validation message.
 *
 * Example:
 * Valid field => { result: 'ok', message: 'Carlos' }
 * Invalid field => { result: 'error', message: 'This field is required' }
 */
function CFValidateField(id, value) {
  var result = {
    result: 'ok',
    message: value,
  };

  switch (id) {
    case 'cf_first_name':
    case 'cf_last_name':
    case 'cf_subject':
    case 'cf_message':
      if (!value) {
        result.result = 'error';
        result.message = CFFormVars.requiredFieldText;
      } else if (id === 'cf_message') {
        if (value.length > 500) {
          result.result = 'error';
          result.message = CFFormVars.tooLong500Text;
        }
      } else if (value.length > 100) {
        result.result = 'error';
        result.message = CFFormVars.tooLong100Text;
      }
      break;

    case 'cf_phone':
      if (!value) {
        result.result = 'error';
        result.message = CFFormVars.requiredFieldText;
      } else if (!CFIsNumeric(value)) {
        result.result = 'error';
        result.message = CFFormVars.invalidPhone;
      }
      break;

    case 'cf_email':
      if (!value) {
        result.result = 'error';
        result.message = CFFormVars.requiredFieldText;
      } else if (!CFIsEmail(value)) {
        result.result = 'error';
        result.message = CFFormVars.invalidEmailText;
      } else if (value.length > 100) {
        result.result = 'error';
        result.message = CFFormVars.tooLong100Text;
      }
      break;

    default:
      break;
  }

  return result;
}

/**
 * Validates the form fields
 *
 * @returns {object} if there are invalid field values
 * then it returns an object with the validation messages,
 * otherwise it returns an object with the field values.
 *
 * Example:
 * Valid fields => { result: 'ok', fields: { cf_first_name: 'Carlos'.... } }
 * Invalid fields => { result: 'error', fields: { cf_first_name: 'This field is required'... } }
 */
function CFValidateFields() {
  var fieldValues = {
    result: 'ok',
    fields: {},
  };

  var errorMessages = {
    result: 'error',
    fields: {},
  };

  fieldValues.fields.cf_first_name = $(CFActiveFormSelector + '#cf_first_name').val();
  fieldValues.fields.cf_last_name = $(CFActiveFormSelector + '#cf_last_name').val();
  fieldValues.fields.cf_email = $(CFActiveFormSelector + '#cf_email').val();
  fieldValues.fields.cf_phone = $(CFActiveFormSelector + '#cf_phone').val();
  fieldValues.fields.cf_subject = $(CFActiveFormSelector + '#cf_subject').val();
  fieldValues.fields.cf_message = $(CFActiveFormSelector + '#cf_message').val();

  var fieldEntries = Object.entries(fieldValues.fields);
  var validationResult = {};

  for (var i = 0; i < fieldEntries.length; i += 1) {
    var fieldId = fieldEntries[i][0];
    var fieldValue = fieldEntries[i][1];

    validationResult = CFValidateField(fieldId, fieldValue);
    switch (validationResult.result) {
      case 'error':
        errorMessages.fields[fieldId] = validationResult.message;
        break;
      case 'ok':
      default:
        break;
    }
  }

  if (Object.keys(errorMessages.fields).length) {
    return errorMessages;
  }

  return fieldValues;
}

/**
 * Sends the form data to the server
 *
 * @param {string} ajaxUrl WP AJAX url
 * @param {object} formData Form data
 * @param {function} onSuccess Called when the ajax request is successful
 * @param {function} onError Called when the ajax request fails
 * @returns void
 */
function CFSendData(ajaxUrl, formData, onSuccess, onError) {
  $.ajax({
    url: ajaxUrl,
    type: 'post',
    data: formData,
    dateType: 'json',
    timeout: 30000,
    success: onSuccess,
    error: function (error, status, errorMessage) {
      console.log(errorMessage);
      onError(CFFormVars.systemErrorText, 'cf_system_error');
    },
  });
}

/**
 * Shows the loader
 *
 * @returns void
 */
function CFShowLoader() {
  $(CFActiveFormSelector + '.cf_loader').removeClass('hidden');
}

/**
 * Hides the loader
 *
 * @returns void
 */
function CFHideLoader() {
  $(CFActiveFormSelector + '.cf_loader').addClass('hidden');
}

/**
 * Shows the result message
 *
 * @param {string} message Message to be shown in the result.
 * @param {string} messageClass CSS class assigned to the container element of the message.
 * @returns void
 */
function CFShowResult(message, messageClass) {
  $(CFActiveFormSelector + '.cf_result').removeClass('hidden');
  $(CFActiveFormSelector + '.cf_result p').html(message);
  $(CFActiveFormSelector + '.cf_result p').attr('class', messageClass);
  $(CFActiveFormSelector + '.cf_form_wrapper').addClass('hidden');
}

/**
 * Hides the result message
 *
 * @param {string} cfFormId
 * @returns void
 */
function CFHideResult(cfFormId) {
  setActiveFormId(cfFormId);
  $(CFActiveFormSelector + '.cf_result').addClass('hidden');
  $(CFActiveFormSelector + '.cf_form_wrapper').removeClass('hidden');
}

/**
 * Resets the form
 *
 * @returns void
 */
function CFResetForm() {
  $(CFActiveFormSelector + '#cf_first_name').val('');
  $(CFActiveFormSelector + '#cf_last_name').val('');
  $(CFActiveFormSelector + '#cf_email').val('');
  $(CFActiveFormSelector + '#cf_subject').val('');
  $(CFActiveFormSelector + '#cf_message').val('');
  $(CFActiveFormSelector + '.cf_error').remove();
}

/**
 * Shows the validation error messages below every invalid field
 *
 * @param {object} Object containing the error messages
 * @returns void
 */
function CFShowErrors(errorMessages) {
  // Remove existing error messages
  $(CFActiveFormSelector + '.cf_error').remove();

  const arrayErrorMessages = Object.entries(errorMessages);

  for (var i = 0; i < arrayErrorMessages.length; i += 1) {
    var fieldId = arrayErrorMessages[i][0];
    var errorMessage = arrayErrorMessages[i][1];

    $(CFActiveFormSelector + '#' + fieldId).after('<p class="cf_error">' + errorMessage + '</p>');
  }
}

/**
 * AJAX success callback
 *
 * @param {object} response Object containing the response from the server.
 * @returns void
 */
function CFOnSuccess(response) {
  switch (response.result) {
    case 'ok':
      CFShowResult(CFFormVars.successText, 'cf_success');
      CFResetForm();
      break;

    case 'error':
      CFShowErrors(response.fields);
      break;

    default:
      break;
  }
}

/**
 * Form submit callback
 *
 * @param {string} cfFormId
 * @returns void
 */
function CFOnSubmit(cfFormId) {
  setActiveFormId(cfFormId);

  var validationResult = CFValidateFields();
  var formData;

  if (validationResult.result === 'ok') {
    formData = validationResult.fields;
    formData.nonce = CFFormVars.nonce;
    formData.action = 'cf_process_form_data';
    CFSendData(CFFormVars.ajaxUrl, formData, CFOnSuccess, CFShowResult);
    return;
  }

  CFShowErrors(validationResult.fields);
}

/**
 * Attaches event handlers for every .cf_form element.
 * (submit handlers for the forms and click handlers for the back buttons)
 *
 * @return void
 */
function CFAttachEventHandlers() {
  $('.cf_form_parent').each(function () {
    var cfFormId = this.id;

    $('.cf_form_parent#' + cfFormId + ' .cf_form').submit(function (event) {
      event.preventDefault();
      CFOnSubmit(cfFormId);
    });

    $('.cf_form_parent#' + cfFormId + ' .cf_back').click(function (event) {
      event.preventDefault();
      CFHideResult(cfFormId);
    });
  });
}

$(document).ready(CFAttachEventHandlers);
