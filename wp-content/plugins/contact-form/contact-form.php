<?php
/**
 * Plugin Name: Contact Form
 * Description: A simple contact form
 * Author:      Carlos Alvarez
 * Version:     1.0.0
 * Text Domain: cf
 *
 * @package CF
 */

// Prevents direct access to this file.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

define( 'CF_VERSION', get_file_data( __FILE__, array( 'Version' ) )[0] );
define( 'CF_PATH', plugin_dir_path( __FILE__ ) );
define( 'CF_INC_PATH', CF_PATH . 'includes/' );
define( 'CF_URL', plugin_dir_url( __FILE__ ) );
define( 'CF_ASSETS_URL', CF_URL . 'assets/' );
define( 'CF_TABLE_NAME', 'cf_form_data' );


/**
 * Main class of the plugin
 */
class CF_MainClass {

	/**
	 * Single class instance
	 *
	 * @var CF_MainClass
	 */
	protected static $instance = null;

	/**
	 * Registers the activation hook and add an action to add the shortcodes
	 * in the 'init' hook
	 */
	private function __construct() {
		register_activation_hook( __FILE__, array( $this, 'activate' ) );
		add_action( 'init', array( $this, 'add_shortcodes' ) );
	}

	/**
	 * Add the shortcodes [cef_form]
	 *
	 * @return void
	 */
	public function add_shortcodes() {
		include_once CF_INC_PATH . 'models/cf-data.php';
		include_once CF_INC_PATH . 'shortcodes/form.php';

		new CF_Form_Shortcode();
	}

	/**
	 * Create database table
	 *
	 * @return void
	 */
	public function activate() {
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();

		$table_name = $this->get_table_name();

		if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) !== $table_name ) {

			$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			  id INT NOT NULL AUTO_INCREMENT,
			  first_name VARCHAR(100),
			  last_name VARCHAR(100),
			  phone VARCHAR(20),
			  email VARCHAR(100),
			  subject VARCHAR(100),
			  message TEXT,
			  date DATETIME,
			  PRIMARY KEY  (id),
			  KEY email_key (email),
			  KEY date_key (date)) $charset_collate";

			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			dbDelta( $sql );
		}
	}

	/**
	 * Returns the database table name
	 *
	 * @return string
	 */
	public function get_table_name() {
		global $wpdb;
		return $wpdb->prefix . CF_TABLE_NAME;
	}

	/**
	 * An unique ID is generated for every form and data list.
	 * This is to avoid problems when two forms or two data lists are in
	 * the same page
	 *
	 * @return string An Unique ID
	 * @throws Exception When the functions 'random_bytes' and
	 * 'openssl_random_pseudo_bytes' are not defined.
	 */
	public function generate_unique_id() {
		$lenght = 13;
		if ( function_exists( 'random_bytes' ) ) {
			$bytes = random_bytes( ceil( $lenght / 2 ) );
		} elseif ( function_exists( 'openssl_random_pseudo_bytes' ) ) {
			$bytes = openssl_random_pseudo_bytes( ceil( $lenght / 2 ) );
		} else {
			throw new Exception( 'no cryptographically secure random function available' );
		}
		return 'cf_' . substr( bin2hex( $bytes ), 0, $lenght );
	}

	/**
	 * Ensures only one instance of CEF is created
	 *
	 * @return CF_MainClass
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

}

/**
 * Returns the CF_MainClass instance
 *
 * @return CF_MainClass CF instance
 */
function CF() {
	return CF_MainClass::instance();
}

CF();
