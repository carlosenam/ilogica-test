<?php
/**
 * Class TestCF_MainClass
 *
 * @package CF
 */

/**
 * Test class for CF_MainClass
 */
class TestCF_MainClass extends WP_UnitTestCase {

	/**
	 * Remove the filter for temporary tables
	 *
	 * @return void
	 */
	public function setup() {
		parent::setup();
		remove_filter( 'query', array( $this, '_create_temporary_tables' ) );
		remove_filter( 'query', array( $this, '_drop_temporary_tables' ) );
	}

	/**
	 * Delete the cf_form_data table from test database
	 *
	 * @return void
	 */
	public function tearDown() {
		parent::tearDown();

		global $wpdb;
		$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}cf_form_data" );
	}

	/**
	 * Test if global variables are defined and have the correct values
	 *
	 * @return void
	 */
	public function test_global_variables_are_defined_and_have_the_correct_values() {
		$this->assertTrue( defined( 'CF_VERSION' ) );
		$this->assertSame( CF_VERSION, '1.0.0' );
		$this->assertTrue( defined( 'CF_PATH' ) );
		$this->assertRegExp( '/wp-content\/plugins\/contact-form\/$/', CF_PATH );
		$this->assertTrue( defined( 'CF_INC_PATH' ) );
		$this->assertSame( CF_INC_PATH, CF_PATH . 'includes/' );
		$this->assertTrue( defined( 'CF_URL' ) );
		$this->assertSame( CF_URL, plugin_dir_url( CF_PATH . 'contact-form.php' ) );
		$this->assertTrue( defined( 'CF_ASSETS_URL' ) );
		$this->assertSame( CF_ASSETS_URL, CF_URL . 'assets/' );
	}


	/**
	 * Test if the shortcodes are added
	 *
	 * @return void
	 */
	public function test_shortcodes_are_added() {
		global $shortcode_tags;

		CF();

		$cf_form = 'cf_form';

		$this->assertTrue( has_action( 'init', array( CF(), 'add_shortcodes' ) ) ? true : false );
		$this->assertArrayHasKey( $cf_form, $shortcode_tags );
		$this->assertInstanceOf( 'CF_Form_Shortcode', $shortcode_tags[ $cf_form ][0] );
		$this->assertSame( 'html_output', $shortcode_tags[ $cf_form ][1] );
	}

	/**
	 * Test if AJAX callbacks are added when no admin
	 *
	 * @return void
	 */
	public function test_ajax_callbacks_are_added_when_no_admin() {
		global $wp_filter;
		CF();
		$process_data_callback = array_shift( $wp_filter['wp_ajax_cf_process_form_data']->callbacks[10] )['function'];
		$this->assertInstanceOf( 'CF_Form_Shortcode', $process_data_callback[0] );
		$this->assertSame( $process_data_callback[1], 'process_form_data' );

		$process_data_callback_no_priv = array_shift( $wp_filter['wp_ajax_nopriv_cf_process_form_data']->callbacks[10] )['function'];
		$this->assertInstanceOf( 'CF_Form_Shortcode', $process_data_callback_no_priv[0] );
		$this->assertSame( $process_data_callback_no_priv[1], 'process_form_data' );
	}

	/**
	 * Test if the plugin activation hook is activated
	 *
	 * @return void
	 */
	public function test_activation_hook_is_added() {
		$plugin_file_path = CF_PATH . 'contact-form.php';
		$this->assertTrue( has_action( 'activate_' . plugin_basename( $plugin_file_path ), array( CF(), 'activate' ) ) ? true : false );
	}

	/**
	 * Test if the 'cf_form data' table is created in the database
	 *
	 * @return void
	 */
	public function test_database_table_is_created() {
		global $wpdb;

		CF()->activate();
		$table_name = $wpdb->prefix . 'cf_form_data';
		$this->assertSame( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ), $table_name );
	}
}
