<?php
/**
 * Class TestCF_Data
 *
 * @package CF
 */

/**
 * Test class for CF_Data
 */
class TestCF_Data extends WP_UnitTestCase {

	/**
	 * Remove the filter for temporary tables
	 *
	 * @return void
	 */
	public function setup() {
		parent::setup();
		remove_filter( 'query', array( $this, '_create_temporary_tables' ) );
		remove_filter( 'query', array( $this, '_drop_temporary_tables' ) );
	}

	/**
	 * Test set_data when all fields are set
	 *
	 * @return void
	 */
	public function test_set_data_all_fields() {
		$data = array(
			'first_name' => 'Carlos',
			'last_name'  => 'Alvarez',
			'email'      => 'carlosenam@hotmail.com',
			'phone'      => '123123123',
			'subject'    => 'Testing',
			'message'    => 'This is testing',
		);

		$cf = new CF_Data();
		$cf->set_data( $data );
		$cf_data = $cf->get_data();

		$this->assertArrayHasKey( 'first_name', $cf_data );
		$this->assertSame( $cf_data['first_name'], $data['first_name'] );
		$this->assertArrayHasKey( 'last_name', $cf_data );
		$this->assertSame( $cf_data['last_name'], $data['last_name'] );
		$this->assertArrayHasKey( 'email', $cf_data );
		$this->assertSame( $cf_data['email'], $data['email'] );
		$this->assertArrayHasKey( 'phone', $cf_data );
		$this->assertSame( $cf_data['phone'], $data['phone'] );
		$this->assertArrayHasKey( 'subject', $cf_data );
		$this->assertSame( $cf_data['subject'], $data['subject'] );
		$this->assertArrayHasKey( 'message', $cf_data );
		$this->assertSame( $cf_data['message'], $data['message'] );
	}

	/**
	 * Test set_data when some fields are set
	 *
	 * @return void
	 */
	public function test_set_data_some_fields() {
		$data = array(
			'first_name' => 'Carlos',
			'last_name'  => 'Alvarez',
		);

		$cf = new CF_Data();
		$cf->set_data( $data );
		$cf_data = $cf->get_data();

		$this->assertArrayHasKey( 'first_name', $cf_data );
		$this->assertSame( $cf_data['first_name'], $data['first_name'] );
		$this->assertArrayHasKey( 'last_name', $cf_data );
		$this->assertSame( $cf_data['last_name'], $data['last_name'] );
		$this->assertArrayHasKey( 'email', $cf_data );
		$this->assertSame( '', $cf_data['email'] );
		$this->assertArrayHasKey( 'subject', $cf_data );
		$this->assertSame( '', $cf_data['subject'] );
		$this->assertArrayHasKey( 'message', $cf_data );
		$this->assertSame( '', $cf_data['message'] );
	}

	/**
	 * Test set_data when $data is not array
	 *
	 * @return void
	 */
	public function test_set_data_data_is_not_array() {
		$data = 1;

		$cf = new CF_Data();
		try {
			$cf_data = $cf->set_data( $data );
		} catch ( Exception $e ) {
			$this->assertSame( $e->getMessage(), '$data is not an array' );
		}
	}

	/**
	 * Test save function
	 *
	 * @return void
	 */
	public function test_save() {

		CF()->activate();

		$cf = new CF_Data();
		$cf->set_data(
			array(
				'first_name' => 'Carlos',
				'last_name'  => 'Alvarez',
				'email'      => 'carlosenam@hotmail.com',
				'phone'      => '123123',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			)
		);

		$cf_id = $cf->save();

		$this->assertTrue( gettype( $cf_id ) === 'integer' );

		global $wpdb;

		$table_name = $wpdb->prefix . 'cf_form_data';

		$myed = $wpdb->get_row( "SELECT * FROM $table_name  WHERE id = $cf_id", ARRAY_A );

		$this->assertSame( $myed['first_name'], 'Carlos' );
		$this->assertSame( $myed['last_name'], 'Alvarez' );
		$this->assertSame( $myed['email'], 'carlosenam@hotmail.com' );
		$this->assertSame( $myed['phone'], '123123' );
		$this->assertSame( $myed['subject'], 'Testing' );
		$this->assertSame( $myed['message'], 'This is testing' );
	}

	/**
	 * Store dummy data
	 *
	 * @return array dummy data
	 */
	public function dummy_data() {
		global $wpdb;

		$data = array(
			array(
				'first_name' => 'Carlos',
				'last_name'  => 'Alvarez',
				'email'      => 'carlosenam@hotmail.com',
				'subject'    => 'Testing',
				'phone'      => '123123123',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Dennet',
				'last_name'  => 'Bort',
				'email'      => 'dennet@correo.com',
				'subject'    => 'Testing',
				'phone'      => '123123123',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Fabiola',
				'last_name'  => 'Cats',
				'email'      => 'fabiola@correo.com',
				'subject'    => 'Testing',
				'phone'      => '123123123',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Enrique',
				'last_name'  => 'Dart',
				'email'      => 'enrique@correo.com',
				'subject'    => 'Testing',
				'phone'      => '123123123',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Gary',
				'last_name'  => 'Emac',
				'email'      => 'gary@correo.com',
				'subject'    => 'Testing',
				'phone'      => '123123123',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Harold',
				'last_name'  => 'Ferret',
				'email'      => 'harold@correo.com',
				'subject'    => 'Testing',
				'phone'      => '123123123',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Irene',
				'last_name'  => 'Garot',
				'email'      => 'Irene@correo.com',
				'subject'    => 'Testing',
				'phone'      => '123123123',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Jake',
				'last_name'  => 'Hermes',
				'email'      => 'jake@correo.com',
				'subject'    => 'Testing',
				'phone'      => '123123123',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Katerine',
				'last_name'  => 'Ipsos',
				'email'      => 'katerine@correo.com',
				'subject'    => 'Testing',
				'phone'      => '123123123',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Larry',
				'last_name'  => 'Jefrey',
				'email'      => 'larry@correo.com',
				'subject'    => 'Testing',
				'phone'      => '123123123',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Mery',
				'last_name'  => 'Lako',
				'email'      => 'mery@correo.com',
				'subject'    => 'Testing',
				'phone'      => '123123123',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Nancy',
				'last_name'  => 'Mongomery',
				'email'      => 'nancy@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
		);

		$cf = new CF_Data();

		foreach ( $data as $cf_data ) {
			$cf->set_data( $cf_data );
			$cf->save();
		}

		return $data;
	}
}
