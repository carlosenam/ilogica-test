<?php
/**
 * Class TestAJAX
 *
 * @package CF
 * @group ajax
 * @runTestsInSeparateProcesses
 */
class TestAJAX extends WP_Ajax_UnitTestCase {

	/**
	 * Remove the filter for temporary tables
	 *
	 * @return void
	 */
	public function setup() {
		parent::setup();
		remove_filter( 'query', array( $this, '_create_temporary_tables' ) );
		remove_filter( 'query', array( $this, '_drop_temporary_tables' ) );
		wp_set_current_user( 1 );
	}

	/**
	 * Store dummy data
	 *
	 * @return array dummy data
	 */
	public function dummy_data() {
		global $wpdb;

		CF()->activate();

		$data = array(
			array(
				'first_name' => 'Carlos',
				'last_name'  => 'Alvarez',
				'email'      => 'carlosenam@hotmail.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Dennet',
				'last_name'  => 'Bort',
				'email'      => 'dennet@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Fabiola',
				'last_name'  => 'Cats',
				'email'      => 'fabiola@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Enrique',
				'last_name'  => 'Dart',
				'email'      => 'enrique@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Gary',
				'last_name'  => 'Emac',
				'email'      => 'gary@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Harold',
				'last_name'  => 'Ferret',
				'email'      => 'harold@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Irene',
				'last_name'  => 'Garot',
				'email'      => 'Irene@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Jake',
				'last_name'  => 'Hermes',
				'email'      => 'jake@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Katerine',
				'last_name'  => 'Ipsos',
				'email'      => 'katerine@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Larry',
				'last_name'  => 'Jefrey',
				'email'      => 'larry@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Mery',
				'last_name'  => 'Lako',
				'email'      => 'mery@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Nancy',
				'last_name'  => 'Mongomery',
				'email'      => 'nancy@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
		);

		$ed = new CF_Enquiry_Data();

		foreach ( $data as $ed_data ) {
			$ed->set_data( $ed_data );
			$ed->save();
		}

		return $data;
	}

	/**
	 * Test process_form_data when all fields are valid
	 *
	 * @return void
	 */
	public function test_cf_form_all_fiels_are_valid() {
		$_POST['cf_first_name'] = 'Carlos';
		$_POST['cf_last_name']  = 'Alvarez';
		$_POST['cf_email']      = 'carlosenam@hotmail.com';
		$_POST['cf_phone']      = '123123123';
		$_POST['cf_subject']    = 'Testing';
		$_POST['cf_message']    = 'This is a test';
		$_POST['nonce']          = wp_create_nonce( 'cf_process_form_data' );

		CF()->activate();
		try {
			wp_set_current_user( 1 );
			$this->_handleAjax( 'cf_process_form_data' );
		} catch ( WPAjaxDieContinueException $e ) {}
		$this->assertTrue( isset( $this->_last_response ) );
		$response = json_decode( $this->_last_response );
		$this->assertTrue( isset( $response->result ) );
		$this->assertSame( 'ok', $response->result );
	}

	/**
	 * Test process_form_data when some fields are invalid
	 *
	 * @return void
	 */
	public function test_cf_form_some_fiels_are_invalid() {
		$_POST['cf_first_name'] = 'Carlos';
		$_POST['cf_email']      = '@hotmail.com';
		$_POST['cf_subject']    = 'Testing';
		$_POST['cf_message']    = 'This is a test';
		$_POST['nonce']         = wp_create_nonce( 'cf_process_form_data' );

		CF()->activate();
		try {
			wp_set_current_user( 1 );
			$this->_handleAjax( 'cf_process_form_data' );
		} catch ( WPAjaxDieContinueException $e ) {}
		$this->assertTrue( isset( $this->_last_response ) );
		$response = json_decode( $this->_last_response );
		$this->assertTrue( isset( $response->result ) );
		$this->assertSame( 'error', $response->result );
		$this->assertTrue( isset( $response->fields ) );
		$this->assertTrue( isset( $response->fields->cf_last_name ) );
		$this->assertSame( 'Este campo es obligatorio', $response->fields->cf_last_name );
		$this->assertTrue( isset( $response->fields->cf_email ) );
		$this->assertSame( 'Correo Inválido', $response->fields->cf_email );
	}

	/**
	 * Test process_form_data when some fields are too long
	 *
	 * @return void
	 */
	public function test_cf_form_some_fiels_are_too_long() {
		$_POST['cf_first_name'] = 'asdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasd';
		$_POST['cf_email']      = 'asdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasd';
		$_POST['cf_subject']    = 'asdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasd';
		$_POST['cf_message']    = 'asdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasd';
		$_POST['nonce']          = wp_create_nonce( 'cf_process_form_data' );

		CF()->activate();
		try {
			wp_set_current_user( 1 );
			$this->_handleAjax( 'cf_process_form_data' );
		} catch ( WPAjaxDieContinueException $e ) {}
		$this->assertTrue( isset( $this->_last_response ) );
		$response = json_decode( $this->_last_response );
		$this->assertTrue( isset( $response->result ) );
		$this->assertSame( 'error', $response->result );
		$this->assertTrue( isset( $response->fields ) );
		$this->assertTrue( isset( $response->fields->cf_first_name ) );
		$this->assertSame( 'Muy Largo (max 100 caracteres)', $response->fields->cf_first_name );
		$this->assertTrue( isset( $response->fields->cf_last_name ) );
		$this->assertSame( 'Este campo es obligatorio', $response->fields->cf_last_name );
		$this->assertTrue( isset( $response->fields->cf_email ) );
		$this->assertSame( 'Correo Inválido', $response->fields->cf_email );
		$this->assertTrue( isset( $response->fields->cf_message ) );
		$this->assertSame( 'Muy Largo (max 500 caracteres)', $response->fields->cf_message );
	}
}
