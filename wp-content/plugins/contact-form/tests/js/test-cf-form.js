// Tests are run using just one enquiry form
CFFormId = $('.cf_form_parent').get(0).id;
CFFormSelector = '.cf_form_parent#' + CFFormId + ' ';

// CFIsEmail Function

QUnit.module('CFIsEmail function');

QUnit.test('Valid email', (assert) => {
  const email = 'carlosenam@hotmail.com';
  assert.ok(CFIsEmail(email));
});

QUnit.test('Valid email with trailing spaces', (assert) => {
  const email = '  carlosenam@hotmail.com  ';
  assert.ok(CFIsEmail(email));
});

QUnit.test('Invalid email with no domain', (assert) => {
  const email = 'carlosenam@';
  assert.notOk(CFIsEmail(email));
});

QUnit.test('Invalid email with no @', (assert) => {
  const email = 'carlosenamhotmail.com';
  assert.notOk(CFIsEmail(email));
});

QUnit.test('Invalid email with strange characters', (assert) => {
  const email = 'carlos///****!!!!enamhotmail.com';
  assert.notOk(CFIsEmail(email));
});

QUnit.test('Empty string', (assert) => {
  const email = '';
  assert.notOk(CFIsEmail(email));
});

QUnit.test('Invalid email with inner spaces', (assert) => {
  const email = 'carlos  @hotmail.  com';
  assert.notOk(CFIsEmail(email));
});

// CFisNumeric

// CFValidateFields Function

QUnit.module('CFValidateFields function', {
  after: () => {
    $('#cf_first_name').val('');
    $('#cf_last_name').val('');
    $('#cf_phone').val('');
    $('#cf_email').val('');
    $('#cf_subject').val('');
    $('#cf_message').val('');
  },
});

QUnit.test('All fields are valid', (assert) => {
  $('#cf_first_name').val('Carlos');
  $('#cf_last_name').val('Alvarez');
  $('#cf_phone').val('12312312');
  $('#cf_email').val('carlosenam@hotmail.com');
  $('#cf_subject').val('Testing');
  $('#cf_message').val('Hi, this is a test');

  const result = CFValidateFields();
  assert.equal(typeof result, 'object');
  assert.equal(typeof result.result, 'string');
  assert.equal(typeof result.fields, 'object');
  assert.equal(typeof result.fields.cf_first_name, 'string');
  assert.equal(typeof result.fields.cf_last_name, 'string');
  assert.equal(typeof result.fields.cf_email, 'string');
  assert.equal(typeof result.fields.cf_phone, 'string');
  assert.equal(typeof result.fields.cf_subject, 'string');
  assert.equal(typeof result.fields.cf_message, 'string');

  assert.equal(result.result, 'ok');
  assert.equal(result.fields.cf_first_name, $('#cf_first_name').val());
  assert.equal(result.fields.cf_last_name, $('#cf_last_name').val());
  assert.equal(result.fields.cf_email, $('#cf_email').val());
  assert.equal(result.fields.cf_phone, $('#cf_phone').val());
  assert.equal(result.fields.cf_subject, $('#cf_subject').val());
  assert.equal(result.fields.cf_message, $('#cf_message').val());
});

QUnit.test('Some fields are invalid', (assert) => {
  $('#cf_first_name').val('');
  $('#cf_last_name').val('Alvarez');
  $('#cf_email').val('carlose/**nam@hotmail.com');
  $('#cf_subject').val('Testing');
  $('#cf_message').val('');

  const result = CFValidateFields();

  assert.equal(typeof result, 'object');
  assert.equal(typeof result.result, 'string');
  assert.equal(typeof result.fields, 'object');
  assert.equal(typeof result.fields.cf_first_name, 'string');
  assert.equal(typeof result.fields.cf_last_name, 'undefined');
  assert.equal(typeof result.fields.cf_email, 'string');
  assert.equal(typeof result.fields.cf_subject, 'undefined');
  assert.equal(typeof result.fields.cf_message, 'string');

  assert.equal(result.result, 'error');
  assert.equal(result.fields.cf_first_name, 'Este campo es obligatorio');
  assert.equal(result.fields.cf_email, 'Correo Inválido');
  assert.equal(result.fields.cf_message, 'Este campo es obligatorio');
});

QUnit.test('All fields are invalid', (assert) => {
  $('#cf_first_name').val('');
  $('#cf_last_name').val('');
  $('#cf_email').val('carlose/**nam@hotmail.com');
  $('#cf_subject').val('');
  $('#cf_message').val('');

  const result = CFValidateFields();

  assert.equal(typeof result, 'object');
  assert.equal(typeof result.result, 'string');
  assert.equal(typeof result.fields, 'object');
  assert.equal(typeof result.fields.cf_first_name, 'string');
  assert.equal(typeof result.fields.cf_last_name, 'string');
  assert.equal(typeof result.fields.cf_email, 'string');
  assert.equal(typeof result.fields.cf_subject, 'string');
  assert.equal(typeof result.fields.cf_message, 'string');

  assert.equal(result.result, 'error');
  assert.equal(result.fields.cf_first_name, 'Este campo es obligatorio');
  assert.equal(result.fields.cf_last_name, 'Este campo es obligatorio');
  assert.equal(result.fields.cf_email, 'Correo Inválido');
  assert.equal(result.fields.cf_subject, 'Este campo es obligatorio');
  assert.equal(result.fields.cf_message, 'Este campo es obligatorio');
});

QUnit.test('Some fields are too long', (assert) => {
  $('#cf_first_name').val('asdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasd');
  $('#cf_last_name').val('asdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasd');
  $('#cf_email').val('carlosenam@hotmail.comasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasd');
  $('#cf_subject').val('asdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasd');
  $('#cf_message').val('asdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasd');

  const result = CFValidateFields();

  assert.equal(typeof result, 'object');
  assert.equal(typeof result.result, 'string');
  assert.equal(typeof result.fields, 'object');
  assert.equal(typeof result.fields.cf_first_name, 'string');
  assert.equal(typeof result.fields.cf_last_name, 'string');
  assert.equal(typeof result.fields.cf_email, 'string');
  assert.equal(typeof result.fields.cf_subject, 'string');
  assert.equal(typeof result.fields.cf_message, 'string');

  assert.equal(result.result, 'error');
  assert.equal(result.fields.cf_first_name, 'Muy Largo (max 100 caracteres)');
  assert.equal(result.fields.cf_last_name, 'Muy Largo (max 100 caracteres)');
  assert.equal(result.fields.cf_email, 'Muy Largo (max 100 caracteres)');
  assert.equal(result.fields.cf_subject, 'Muy Largo (max 100 caracteres)');
  assert.equal(result.fields.cf_message, 'Muy Largo (max 500 caracteres)');
});

// CFValidateField Function

QUnit.module('CFValidateField function');

QUnit.test('Empty field', (assert) => {
  let result = CFValidateField('cf_first_name', '');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'Este campo es obligatorio');
  result = CFValidateField('cf_last_name', '');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'Este campo es obligatorio');
  result = CFValidateField('cf_subject', '');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'Este campo es obligatorio');
  result = CFValidateField('cf_message', '');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'Este campo es obligatorio');
  result = CFValidateField('cf_email', '');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'Este campo es obligatorio');
  result = CFValidateField('cf_phone', '');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'Este campo es obligatorio');
});

QUnit.test('Invalid email', (assert) => {
  let result = CFValidateField('cf_email', 'carlos///asdasd');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'Correo Inválido');
});

QUnit.test('Invalid phone', (assert) => {
  let result = CFValidateField('cf_phone', 'asdasdas123123');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'Teléfono Inválido');
});

QUnit.test('Valid Field', (assert) => {
  let result = CFValidateField('cf_first_name', 'Carlos');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'ok');
  assert.equal(result.message, 'Carlos');
  result = CFValidateField('cf_last_name', 'Alvarez');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'ok');
  assert.equal(result.message, 'Alvarez');
  result = CFValidateField('cf_subject', 'Testing');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'ok');
  assert.equal(result.message, 'Testing');
  result = CFValidateField('cf_message', 'This is a test');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'ok');
  assert.equal(result.message, 'This is a test');
  result = CFValidateField('cf_email', 'carlosenam@hotmail.com');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'ok');
  assert.equal(result.message, 'carlosenam@hotmail.com');
  result = CFValidateField('cf_phone', '123123123');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'ok');
  assert.equal(result.message, '123123123');
});

// CFShowErrors Function

QUnit.module('CFShowErrors function');

QUnit.test('All fields are invalid', (assert) => {
  const errorMessages = {
    cf_first_name: 'Este campo es obligatorio',
    cf_last_name: 'Este campo es obligatorio',
    cf_email: 'Correo Inválido',
    cf_phone: 'Teléfono Inválido',
    cf_subject: 'Este campo es obligatorio',
    cf_message: 'Este campo es obligatorio',
  };

  CFShowErrors(errorMessages);

  assert.ok($(CFFormSelector + '#cf_first_name').next().hasClass('cf_error'));
  assert.ok($(CFFormSelector + '#cf_last_name').next().hasClass('cf_error'));
  assert.ok($(CFFormSelector + '#cf_email').next().hasClass('cf_error'));
  assert.ok($(CFFormSelector + '#cf_phone').next().hasClass('cf_error'));
  assert.ok($(CFFormSelector + '#cf_subject').next().hasClass('cf_error'));
  assert.ok($(CFFormSelector + '#cf_message').next().hasClass('cf_error'));

  assert.equal($(CFFormSelector + '#cf_first_name').next().html(), 'Este campo es obligatorio');
  assert.equal($(CFFormSelector + '#cf_last_name').next().html(), 'Este campo es obligatorio');
  assert.equal($(CFFormSelector + '#cf_email').next().html(), 'Correo Inválido');
  assert.equal($(CFFormSelector + '#cf_phone').next().html(), 'Teléfono Inválido');
  assert.equal($(CFFormSelector + '#cf_subject').next().html(), 'Este campo es obligatorio');
  assert.equal($(CFFormSelector + '#cf_message').next().html(), 'Este campo es obligatorio');
});

QUnit.test('Some fields are invalid', (assert) => {
  const errorMessages = {
    cf_first_name: 'Este campo es obligatorio',
    cf_email: 'Correo Inválido',
    cf_message: 'Este campo es obligatorio',
  };

  CFShowErrors(errorMessages);

  assert.ok($(CFFormSelector + '#cf_first_name').next().hasClass('cf_error'));
  assert.notOk($(CFFormSelector + '#cf_last_name').next().hasClass('cf_error'));
  assert.ok($(CFFormSelector + '#cf_email').next().hasClass('cf_error'));
  assert.notOk($(CFFormSelector + '#cf_subject').next().hasClass('cf_error'));
  assert.ok($(CFFormSelector + '#cf_message').next().hasClass('cf_error'));

  assert.equal($(CFFormSelector + '#cf_first_name').next().html(), 'Este campo es obligatorio');
  assert.notEqual($(CFFormSelector + '#cf_last_name').next().html(), 'Este campo es obligatorio');
  assert.equal($(CFFormSelector + '#cf_email').next().html(), 'Correo Inválido');
  assert.notEqual($(CFFormSelector + '#cf_subject').next().html(), 'Este campo es obligatorio');
  assert.equal($(CFFormSelector + '#cf_message').next().html(), 'Este campo es obligatorio');
});

QUnit.test('Empty errorMessages', (assert) => {
  const errorMessages = {};
  CFShowErrors(errorMessages);

  assert.notOk($(CFFormSelector + '#cf_first_name').next().hasClass('cf_error'));
  assert.notOk($(CFFormSelector + '#cf_last_name').next().hasClass('cf_error'));
  assert.notOk($(CFFormSelector + '#cf_email').next().hasClass('cf_error'));
  assert.notOk($(CFFormSelector + '#cf_subject').next().hasClass('cf_error'));
  assert.notOk($(CFFormSelector + '#cf_message').next().hasClass('cf_error'));
});

// CFOnSuccess Function

QUnit.module('CFOnSucsess function');

QUnit.test('Form data is stored succesfully', (assert) => {
  const response = {
    result: 'ok',
  };

  CFOnSuccess(response);
  assert.ok($(CFFormSelector + '.cf_loader').hasClass('hidden'));
  assert.notOk($(CFFormSelector + '.cf_result').hasClass('hidden'));
});

QUnit.test('Server returns error messages', (assert) => {
  const response = {
    result: 'error',
    fields: {
      cf_email: 'Correo Inválido',
    },
  };

  CFOnSuccess(response);

  assert.ok($(CFFormSelector + '.cf_loader').hasClass('hidden'));
  assert.ok($(CFFormSelector + '#cf_email').next().hasClass('cf_error'));
});

// Loader Module

QUnit.module('Loader');

QUnit.test('Show loader', (assert) => {
  CFShowLoader();

  assert.notOk($(CFFormSelector + '.cf_loader').hasClass('hidden'));
});

QUnit.test('Hide loader', (assert) => {
  CFHideLoader();

  assert.ok($(CFFormSelector + '.cf_loader').hasClass('hidden'));
});

// Show Results Module

QUnit.module('Show Results');

QUnit.test('Show succesful result', (assert) => {
  const resultMsg = 'Thank you for sending us your feedback';
  const msgClass = 'cf_success';

  CFShowResult(resultMsg, msgClass);

  assert.notOk($(CFFormSelector + '.cf_result').hasClass('hidden'));
  assert.equal($(CFFormSelector + '.cf_result p').html(), 'Thank you for sending us your feedback');
  assert.ok($(CFFormSelector + '.cf_result p').hasClass('cf_success'));
  assert.ok($(CFFormSelector + '.cf_form_wrapper').hasClass('hidden'));
});

QUnit.test('Show system error message', (assert) => {
  const resultMsg = 'System error: Please try again later';
  const msgClass = 'cf_system_error';

  CFShowResult(resultMsg, msgClass);

  assert.notOk($(CFFormSelector + '.cf_result').hasClass('hidden'));
  assert.equal($(CFFormSelector + '.cf_result p').html(), 'System error: Please try again later');
  assert.ok($(CFFormSelector + '.cf_result p').hasClass('cf_system_error'));
  assert.ok($(CFFormSelector + '.cf_form_wrapper').hasClass('hidden'));
});

QUnit.test('Hide successful result', (assert) => {
  CFHideResult(CFFormId);

  assert.ok($(CFFormSelector + '.cf_result').hasClass('hidden'));
  assert.notOk($(CFFormSelector + '.cf_form_wrapper').hasClass('hidden'));
});

// CFResetForm Function

QUnit.module('resetForm function');

QUnit.test('Form have some valid inputs', (assert) => {
  $('#cf_first_name').val('');
  $('#cf_last_name').val('Alvarez');
  $('#cf_email').val('carlose/**nam@hotmail.com');
  $('#cf_subject').val('Testing');
  $('#cf_message').val('');

  CFResetForm();

  assert.equal('', $('#cf_first_name').val());
  assert.equal('', $('#cf_last_name').val());
  assert.equal('', $('#cf_email').val());
  assert.equal('', $('#cf_subject').val());
  assert.equal('', $('#cf_message').val());
  assert.equal(0, $('.cf_error').length);
});

QUnit.test('Form have all valid inputs', (assert) => {
  $('#cf_first_name').val('Carlos');
  $('#cf_last_name').val('Alvarez');
  $('#cf_email').val('carlosenam@hotmail.com');
  $('#cf_subject').val('Testing');
  $('#cf_message').val('This is testing');

  CFResetForm();

  assert.equal('', $('#cf_first_name').val());
  assert.equal('', $('#cf_last_name').val());
  assert.equal('', $('#cf_email').val());
  assert.equal('', $('#cf_subject').val());
  assert.equal('', $('#cf_message').val());
  assert.equal(0, $('.cf_error').length);
});

// CFAttachEventHandlers Function

QUnit.module('CFAttachEventHandlers function');

QUnit.test('Submit handler is attached to the form', (assert) => {
  CFAttachEventHandlers();
  assert.equal(typeof $._data( $('.cf_form')[0], 'events' ).submit, 'object');
});

QUnit.test('Submit handler is attached to the form', (assert) => {
  CFAttachEventHandlers();
  assert.equal(typeof $._data( $('.cf_back')[0], 'events' ).click, 'object');
});
