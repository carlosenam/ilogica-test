<?php
/**
 * Class TestCF_Form_Shortcode
 *
 * @package CF
 */

/**
 * Test Class for the CF_Form_Shortcode class
 */
class TestCF_Form_Shortcode extends WP_UnitTestCase {

	/**
	 * Test if the scripts and styles are enqueued for [cf_form]
	 *
	 * @return void
	 */
	public function test_scripts_and_styles_are_enqueued() {

		$cf_form_shortcode = new CF_Form_Shortcode();

		$cf_form_shortcode->enqueue_scripts_and_styles();

		$scripts_object = array(
			array(
				'id'   => 'cf-form',
				'url'  => CF_ASSETS_URL . 'js/cf-form.js',
				'type' => 'script',
			),
			array(
				'id'   => 'cf-form-styles',
				'url'  => CF_ASSETS_URL . 'css/cf-form.css',
				'type' => 'style',
			),
		);

		$result = true;

		foreach ( $scripts_object as $script_object ) {
			$id   = $script_object['id'];
			$url  = $script_object['url'];
			$type = $script_object['type'];
			if ( ! $this->script_is_enqueued( $id, $url, $type ) ) {
				$result = false;
				break;
			}
		}

		$this->assertTrue( $result );

		// Test localized scripts.
		global $wp_scripts;
		$data               = $wp_scripts->get_data( 'cf-form', 'data' );
		$data               = substr( $data, strpos( $data, '{' ) - 1, -1 );
		$cf_form_vars_data = json_decode( $data, true );
		$this->assertCount( 9, $cf_form_vars_data );
		$this->assertarrayHasKey( 'ajaxUrl', $cf_form_vars_data );
		$this->assertarrayHasKey( 'nonce', $cf_form_vars_data );
		$this->assertarrayHasKey( 'successText', $cf_form_vars_data );
		$this->assertarrayHasKey( 'systemErrorText', $cf_form_vars_data );
		$this->assertarrayHasKey( 'requiredFieldText', $cf_form_vars_data );
		$this->assertarrayHasKey( 'tooLong500Text', $cf_form_vars_data );
		$this->assertarrayHasKey( 'tooLong100Text', $cf_form_vars_data );
		$this->assertarrayHasKey( 'invalidEmailText', $cf_form_vars_data );
		$this->assertarrayHasKey( 'invalidPhone', $cf_form_vars_data );
	}

	/**
	 * Test the validate_form_data function when all field values are valid
	 *
	 * @return void
	 */
	public function test_validate_form_data_all_fields_are_valid() {
		$cf_form_shortcode = new CF_Form_Shortcode();

		$post_data = array(
			'cf_first_name' => 'Carlos',
			'cf_last_name'  => 'Alvarez',
			'cf_email'      => 'carlosenam@hotmail.com',
			'cf_phone'      => '13123123',
			'cf_subject'    => 'Testing',
			'cf_message'    => 'This is testing',
		);

		$validation_result = $cf_form_shortcode->validate_form_data( $post_data );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'fields', $validation_result );
		$this->assertArrayHasKey( 'first_name', $validation_result['fields'] );
		$this->assertArrayHasKey( 'last_name', $validation_result['fields'] );
		$this->assertArrayHasKey( 'email', $validation_result['fields'] );
		$this->assertArrayHasKey( 'phone', $validation_result['fields'] );
		$this->assertArrayHasKey( 'subject', $validation_result['fields'] );
		$this->assertArrayHasKey( 'message', $validation_result['fields'] );
		$this->assertSame( $post_data['cf_first_name'], $validation_result['fields']['first_name'] );
		$this->assertSame( $post_data['cf_last_name'], $validation_result['fields']['last_name'] );
		$this->assertSame( $post_data['cf_email'], $validation_result['fields']['email'] );
		$this->assertSame( $post_data['cf_phone'], $validation_result['fields']['phone'] );
		$this->assertSame( $post_data['cf_subject'], $validation_result['fields']['subject'] );
		$this->assertSame( $post_data['cf_message'], $validation_result['fields']['message'] );
	}

	/**
	 * Test the validate_form_data function some field values are invalid
	 *
	 * @return void
	 */
	public function test_validate_form_data_some_fields_are_invalid() {
		$cf_form_shortcode = new CF_Form_Shortcode();

		$post_data = array(
			'cf_first_name' => 'Carlos',
			'cf_last_name'  => '',
			'cf_email'      => 'carlosenam@hotmail.  com',
			'cf_phone'      => '12312qas3123',
			'cf_subject'    => '',
			'cf_message'    => 'This is testing',
		);

		$validation_result = $cf_form_shortcode->validate_form_data( $post_data );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'fields', $validation_result );
		$this->assertFalse( isset( $validation_result['fields']['first_name'] ) );
		$this->assertArrayHasKey( 'cf_last_name', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cf_email', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cf_phone', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cf_subject', $validation_result['fields'] );
		$this->assertFalse( isset( $validation_result['fields']['message'] ) );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['fields']['cf_last_name'] );
		$this->assertSame( 'Correo Inválido', $validation_result['fields']['cf_email'] );
		$this->assertSame( 'Teléfono Inválido', $validation_result['fields']['cf_phone'] );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['fields']['cf_subject'] );
	}

	/**
	 * Test the validate_form_data function some field values are undefined
	 *
	 * @return void
	 */
	public function test_validate_form_data_some_fields_are_undefined() {
		$cf_form_shortcode = new CF_Form_Shortcode();

		$post_data = array(
			'cf_first_name' => 'Carlos',
			'cf_message'    => 'This is testing',
		);

		$validation_result = $cf_form_shortcode->validate_form_data( $post_data );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'fields', $validation_result );
		$this->assertFalse( isset( $validation_result['fields']['first_name'] ) );
		$this->assertArrayHasKey( 'cf_last_name', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cf_email', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cf_subject', $validation_result['fields'] );
		$this->assertFalse( isset( $validation_result['fields']['message'] ) );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['fields']['cf_last_name'] );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['fields']['cf_email'] );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['fields']['cf_subject'] );
	}

	/**
	 * Test the validate_form_data function all field values are undefined
	 *
	 * @return void
	 */
	public function test_validate_form_data_all_fields_are_undefined() {
		$cf_form_shortcode = new CF_Form_Shortcode();

		$post_data = array();

		$validation_result = $cf_form_shortcode->validate_form_data( $post_data );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'fields', $validation_result );
		$this->assertFalse( isset( $validation_result['fields']['first_name'] ) );
		$this->assertArrayHasKey( 'cf_last_name', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cf_email', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cf_subject', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cf_message', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cf_first_name', $validation_result['fields'] );
		$this->assertFalse( isset( $validation_result['fields']['message'] ) );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['fields']['cf_last_name'] );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['fields']['cf_email'] );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['fields']['cf_subject'] );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['fields']['cf_message'] );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['fields']['cf_first_name'] );
	}

	/**
	 * Test the validate_single_form_field function when the field is empty
	 *
	 * @return void
	 */
	public function test_validate_single_form_field_when_empty_field() {
		$cf_form_shortcode = new CF_Form_Shortcode();

		$id    = 'first_name';
		$value = '';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['message'] );

		$id = 'last_name';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['message'] );

		$id = 'email';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['message'] );

		$id = 'phone';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['message'] );

		$id = 'subject';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['message'] );

		$id = 'message';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'Este campo es obligatorio', $validation_result['message'] );
	}

	/**
	 * Test the validate_single_form_field function when an email is invalid
	 *
	 * @return void
	 */
	public function test_validate_single_form_field_when_invalid_email() {
		$cf_form_shortcode = new CF_Form_Shortcode();

		$id    = 'email';
		$value = 'carlos@_|@@';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'Correo Inválido', $validation_result['message'] );
	}

	/**
	 * Test the validate_single_form_field function when the field value is valid
	 *
	 * @return void
	 */
	public function test_validate_single_form_field_when_valid_field_value() {
		$cf_form_shortcode = new CF_Form_Shortcode();

		$id    = 'Carlos';
		$value = 'first_name';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( $value, $validation_result['message'] );

		$id    = 'last_name';
		$value = 'Alvarez';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( $value, $validation_result['message'] );

		$id    = 'email';
		$value = 'carlos@hotmail.com';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( $value, $validation_result['message'] );

		$id    = 'phone';
		$value = '112312';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( $value, $validation_result['message'] );

		$id    = 'subject';
		$value = 'Testing';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( $value, $validation_result['message'] );

		$id    = 'message';
		$value = 'This is a test';

		$validation_result = $cf_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( $value, $validation_result['message'] );
	}

	/**
	 * Return true if a script/style is enqueued
	 *
	 * @param string $script_id id of the enqueued script/style.
	 * @param string $script_url url of the enqueued script/style.
	 * @param string $script_type it can be 'script' or 'style'.
	 * @return boolean
	 */
	public function script_is_enqueued( $script_id, $script_url, $script_type = 'script' ) {
		$script_array = [];
		global $wp_styles, $wp_scripts;
		$script_object;
		switch ( $script_type ) {
			case 'script':
				$script_object = $wp_scripts;
				break;
			case 'style':
				$script_object = $wp_styles;
				break;
		}

		foreach ( $script_object->queue as $script ) {
			if ( $script_object->registered[ $script ]->src === $script_url
				&& $script_id === $script ) {
				return true;
			}
		}

		return false;
	}

}
