<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ilogica' );

/** MySQL database username */
define( 'DB_USER', 'phpmyadmin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'm|NdEe,,i^u]%N(d6. Ay!U_2ce|Fw$&8DA8JVfXlCGXjo^f[^a{E+f%vSH1yt&L' );
define( 'SECURE_AUTH_KEY',  'PEAk*|r<f0z1K3;y|)-7h4PI/U>51wUPhqv.`(avm6GV2l1zmKs|[]4oHFD7M[`E' );
define( 'LOGGED_IN_KEY',    'Pml`{UGRl2 {aEzme5dK9#|*%[L+eMc2FQwZhK8}IUME+Dh(d/SDrc6Rq}[]##*>' );
define( 'NONCE_KEY',        '?G[XCU4O|L %5Vc9WIaV;q+,UKjf>W8E(C:a.8f{v}|Jv.>XMbLMi)U#!;UcqhoE' );
define( 'AUTH_SALT',        '1vQ?Y,soDV?#rKdBZk^-nt<Tr1uGh=6zX8+2*DTw$FKJ~wQ>P)x>zLr]K1V2.E&N' );
define( 'SECURE_AUTH_SALT', 'WK4pc+CAX6yrVRdG<$B_$R+PaFB4|q8t?m&wv*P#x=jiVOb1I2O+_`L~zJhux`PF' );
define( 'LOGGED_IN_SALT',   '[{<AIM!%xy%*Mz4&rCc%dQ aW&q-f-vWP]mt043)sS1_A9_`X*?/~.<bwA>`s0k]' );
define( 'NONCE_SALT',       'I%FTi[[J]Ve!}CK*MH,9p^uk& !xyIR~l{5~!OkW,@@ <`hp?s9oo4j;3(HKVZ*9' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
