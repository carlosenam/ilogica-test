# Prueba Ilógica

Para esta prueba desarrollè dos cosas:
 - Un plugin (``contact-form``)
 - Un tema (``ilógica``)

La estructura general del tema está basada en underscores https://underscores.me/. Esta estructura es la que usa en WP CLI.

El plugin ``contact-form``también fue desarollado con la ayuda de WP CLI ya que este comando genera cosas útiles para empezar a desarrollar un tema, como las cabeceras del archivo principal del plugin y las pruebas unitarias. Para este plugin también ``QUnit`` (https://qunitjs.com/) para hacer pruebas unitarias en el frontend.

Notas:
 - El diseño no está 100% terminado ya que no considera responsive.
 - Si quieren ejecutar las pruebas unitarias del plugin, deberán usar ``npm`` y ``composer``: ir a la carpeta del plugin en la terminar y ejecutar: ``npm install`` y ``composer install``, para ejecutar los test de PHP: ``composer test``, para ejecutar las funciones que procesan las solicitudes AJAX del formulario, ejecutar ``composer testajax``, y por último para ejecutar las pruebas de jQuery, hay que descomentar las lineas 123,124 y 125 del archivo ``shortcodes/form.php`` y ver las páginas de contacto  en el navegador, allí aparacerán los resultados de las pruebas.
 - Para preprocesar y comprimir los assets usé ``Grunt`` (https://gruntjs.com/). para usar los comandos de este programa, se debe ir a la carpeta del tema en la terminal y ejecutar: ``npm install``, y luego ``npm run build``.